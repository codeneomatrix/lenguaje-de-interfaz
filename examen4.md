### Examen unidad 4
Los numero felices son aquellos que al multiplicar las cifras que los conforman y sumar esos productos, despues de *n* iteraciones el resulatdo sera 1.
Dado un numero cualquiera (del 10 hasta el 44) introducido por teclado, imprimir en pantalla la serie de numeros que genera este procedimento inlcuyendo el 1 (si es que llega hasta el uno). 

Ejemplo:
(Nota: por alguna razon al imprimir un salto de linea este era interpretado como un caracter extraño, lo UNICO importante es la secuencia de numeros)

Tome en cuenta lo siguiente :
- Los numeros felices se explican en el siguiente video:
https://www.youtube.com/watch?v=R2D8SuRjtnQ
- El numero *n* es un numero entre 10 y 44 , que se introducira por teclado
- Puede utilizar todos los registros que considere necesarios
- Puede utilizar todo el espacio de memoria que considere necesario
- 
Ejemplo:
``` bash
~/Desktop $ ./examen4 
23
2313100101⏎  
~/Desktop $ ./examen4 
44
4432131001⏎  
```

## Condiciones de entrega
Subira el ejecutable a su cuenta de github el cual tendra el siguente nombre :
unidad4[numerodecontrol]
ejemplo
unidad412161219
ademas del codigo.