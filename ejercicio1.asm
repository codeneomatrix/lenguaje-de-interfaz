SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "f vale :", 0xA,0xD
	len1 equ $- msg1
segment .bss
	num1 resb 2

section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point
	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg1
	mov edx, len1
	int 0x80


mov edx, 3
mov eax, 1
mov ebx, 1
mov ecx, 0

mov edi, 0
ciclo:
      mov edi, eax
      add edi, ebx
      mov eax, ebx
      mov ebx, edi

      inc ecx
	cmp ecx, edx
jbe ciclo

    add edi, '0'
	mov [num1], edi
	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 1
	int 0x80

salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80
