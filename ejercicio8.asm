SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	asterisco db "*"
	len1 equ $- asterisco
	salto db 0xA,0xD
	len2 equ $- salto
	espacio db " "
	len3 equ $- espacio
	tronco db "H"
	lentronco equ $- tronco

segment .bss
	num1 resb 2

section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point

mov edi,16
mov ebp, 1
filas:
mov esi, edi
		dibuja:
		    mov eax, SYS_PRINT
			mov ebx, STDOUT
			mov ecx, espacio
			mov edx, len1
			int 0x80
			dec esi
		 cmp esi,0
		 jne dibuja

        mov esi, ebp
        estrellitas:
            mov eax, SYS_PRINT
			mov ebx, STDOUT
			mov ecx, asterisco
			mov edx, len1
			int 0x80
			dec esi
		 cmp esi,0
		 jne estrellitas
    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, salto
	mov edx, len1
	int 0x80
dec edi
add ebp,2
cmp edi,0
ja filas

mov ebp, 5
mono:
mov esi, 13
dibuja_espacios_tronco:
		    mov eax, SYS_PRINT
			mov ebx, STDOUT
			mov ecx, espacio
			mov edx, len1
			int 0x80
			dec esi
		 cmp esi,0
		 jne dibuja_espacios_tronco

mov esi, 6
dibuja_tronco:
            mov eax, SYS_PRINT
			mov ebx, STDOUT
			mov ecx, tronco
			mov edx, lentronco
			int 0x80
            dec esi
		    cmp esi,0
		    jne dibuja_tronco
		    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, salto
	mov edx, len1
	int 0x80

dec ebp
cmp ebp,0
ja mono


salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80