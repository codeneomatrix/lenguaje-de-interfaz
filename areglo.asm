segment .data
arre db '4','5', '2','3', '1','1', 48,'9','2','8'
len equ $-arre

ln db 10,13
lenln equ $-ln

segment .text
global _start
_start:
mov ebp, arre
mov edi, 1
; colocamos la direccion de memoria en el registro ebp
; colocamos el numero que nos servira de contador en el
; registro edi

ciclo:
mov eax, 4
mov ebx, 0
mov ecx, ebp ; le pasamos a ecx la direccion de memoria que esta
;almacenado en el registro ebp,de donde comenzara a imprimir
mov edx, 2
int 80h ; con esto imprimimos en pantalla el valor de dos elementos
; del arreglo
; imprimimos un salto de linea (\n)
mov eax, 4
mov ebx, 0
mov ecx, ln
mov edx, lenln
int 80h

add ebp,3 ; aumentamos en dos el valor de la posición en la memoria
; con el fin de obtener el "segundo" elemento del arreglo "23"
; esto porque el "primer" elemento es "45" un numero de dos
; elementos
add edi, 2 ; aumentamos en dos el valor del contador
cmp edi,len
; preguntamos si ya hemos impreso todos los
; datos del arreglo
jb ciclo

salir:
mov eax, 1
xor ebx, ebx
int 0x80