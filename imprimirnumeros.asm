segment .data
arre db 0,10,6,42,99
len equ $-arre

arre2 db 0,0,0,0,0
len2 equ $-arre

ln db 10,13
lenln equ $-ln

segment .bss
  datos resb 4

segment .text
global _start
_start:
mov ebp, arre
mov edi, 0
; colocamos la direccion de memoria en el registro ebp
; colocamos el numero que nos servira de contador en el
; registro edi


ciclo:

mov ah,0
mov al, [ebp+edi]

mov dl, 10
div dl

add al, '0'
mov [datos+0], al

add ah, '0'
mov [datos+1], ah

mov eax, 4
mov ebx, 0
mov ecx, datos
mov edx, 2
int 80h

mov eax, 4
mov ebx, 0
mov ecx, ln
mov edx, lenln
int 80h

add edi, 1 ; aumentamos en uno el valor del contador
cmp edi,len
; preguntamos si ya hemos impreso todos los
; datos del arreglo
jb ciclo

salir:
mov eax, 1
xor ebx, ebx
int 0x80