SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "ebx vale :", 0xA,0xD
	len1 equ $- msg1
	msg2 db "ingrese numero2", 0xA,0xD
	len2 equ $- msg2

segment .bss
	num1 resb 1


section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point

    mov eax, 1
	cmp eax, 5 ; compara el valor de eax con 5

   je bloque
   mov ebx,0
   add ebx, '0'
   mov [num1], ebx

  jmp otrobloque

bloque: mov ebx, 1
   add ebx, '0'
   mov [num1], ebx

otrobloque:
    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 1
	int 0x80


salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80