SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "eax vale :", 0xA,0xD
	len1 equ $- msg1
	msg2 db "ingrese numero2", 0xA,0xD
	len2 equ $- msg2

segment .bss
	num1 resb 1


section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point



 mov eax, 0
 mov ebx, 1
 mov ecx, 5
 loop_for:
      add eax,ebx
 loop loop_for

    add eax, '0'
    mov [num1], eax

    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg1
	mov edx, len1
	int 0x80

    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 1
	int 0x80


salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80