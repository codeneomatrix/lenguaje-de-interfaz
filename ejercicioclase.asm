SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1
; ((a/b)-3)*c
segment .data

	letrero db  0xA,0xD,"el resultado es : ", 0xA,0xD
	tamanioletrero equ $- letrero
	msg8 db  0xA,0xD,"----------------<>---------------------------", 0xA,0xD
	len8 equ $- msg8

segment .bss
  res resb 1

section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point

;--------------------<division>----------------------------------

	mov eax, 16
	mov ebx, 4
	mov ecx, 2

	div ebx
    sub eax, 1

    mul ecx

	add eax, 48
	mov [res], eax
	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 1
	int 0x80

	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg8
	mov edx, len8
	int 0x80


salir:
	mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80