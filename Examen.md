## Examen unidad 1 y 2
Use la propiedades de las integrales para evaluar
$$\int_0^2 (4+3x-3x^2) \,dx$$
Utilice sumas,restas, multiplicaciones y divisiones para escribir un programe en ensamblador que entrege el resultado.
Recuerde que :
- Las potencias se pueden expresar como multiplicaciones 
$$x^2 = x*x$$     
y
$$x^3 = x*x*x$$

- En ensamblador las divisiones son enteras, osea:
     1/3 = 0
- Puede usar mul y div , usando registros y numeros enteros , osea:
mul eax, ecx   
o    
mul eax, 5

- No es necesaria la lectura de teclado
- Puede utilizar, si asi lo desea todos los espacios de memoria que considere necesario
- si no recuerda como resolver una integral definida, aqui un video https://www.youtube.com/watch?v=V7WnsXYJZaM
### Condiciones de entrega 
Subira el ejecutable a su cuenta de github, el cual tendra el siguiente nombre:
unidad1[numerodecontrol], ejemplo: unidad112161219 ; ademas del codigo. 