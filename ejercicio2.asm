SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "escriba un numero en pantalla :", 0xA,0xD
	len1 equ $- msg1
	msg2 db "El resultado es :", 0xA,0xD
	len2 equ $- msg2
segment .bss
	num1 resb 2

section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point



mov esi, 0
mov ecx, 3


ciclo:
    mov edi, ecx
    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg1
	mov edx, len1
	int 0x80

	mov eax, SYS_LEE
	mov ebx, STDIN
	mov ecx, num1
	mov edx, 2
	int 0x80

	mov ebx, [num1]
	sub ebx, 48

	add esi,ebx

	mov ecx, edi
loop ciclo



    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg2
	mov edx, len2
	int 0x80

    add esi, '0'
	mov [num1], esi
	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 1
	int 0x80

salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80
