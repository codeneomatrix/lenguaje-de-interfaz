segment .data
arre db 0, 0, 0, 0 ; modifique los valores a números dentro
; del rango del 0 a 9 para
; evitar imprimir símbolos
len equ $-arre
msg1 db "ingrese un numero y presione enter, repita el proceso cuatro veces:", 0xA
len1 equ $-msg1

segment .bss
dato resb 2

segment .text
global _start
_start:
mov eax, 4
mov ebx, 1
mov ecx, msg1
mov edx, len1
int 0x80

mov esi, arre
mov edi, 0
ciclo_lectura:
mov eax, 3
mov ebx, 0
mov ecx, dato
mov edx, 3
int 0x80 ; con este conjunto de instrucciones leemos el valor de
; los elementos del arreglo desde el teclado
mov al, [dato]
; accedemos al dato introducido desde el teclado
sub al, '0'
mov [esi+edi], al ;colocamos el dato dentro del arreglo en la
;posision edi (0,1,2,....) en bytes
add edi,1
cmp edi,len
; incrementamos edi en uno
; preguntamos si ya hemos leido todos los
; numeros del arreglo
jb ciclo_lectura
; si aun faltan datos por imprimir damos vuelta
; al ciclo de nuevo y en caso contrario dejamos
; de imprimir en pantalla
mov esi, arre
mov edi, 0


ciclo:
mov al, [esi+edi] ;obtenemos el dato dentro del arreglo en la
;posision ecx (0,1,2,....)

add al, '0'
mov [dato], al
add edi,1
; movemos el dato obtenido a la varieble dato
; incrementamos edi en uno
mov eax, 4
mov ebx, 0
mov ecx, dato
mov edx, 1
int 80h ; con esto imprimios en pantalla el valor de los elementos
; del arreglo
cmp edi,len
jb ciclo
; preguntamos si ya hemos impreso todos los
; numeros del arreglo
; si aun faltan datos por imprimir damos vuelta
; al ciclo de nuevo y en caso contrario dejamos
; de imprimir en pantalla

salir:
mov eax, 1
xor ebx, ebx
int 0x80
