SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data

	msg6 db  0xA,0xD,"el resultado es: "
	len6 equ $- msg6

	msg8 db  0xA,0xD,"----------------<>---------------------------", 0xA,0xD
	len8 equ $- msg8

segment .bss
	res resb 1


section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point
	;suma>----------------------------------
	mov eax, 1
	mov edx, 2
	mov ecx, 1

	mul edx
	add eax,2
    div ecx

	add eax, 48
	mov [res], eax


	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg6
	mov edx, len6
	int 0x80

	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, res
	mov edx, 1
	int 0x80


salir:
	mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80