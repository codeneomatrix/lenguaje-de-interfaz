segment .data
mensaje db "introduzca un numero: "
tamanio equ $-mensaje

arre db 0,0,0,0,0,0
len equ $-arre

ln db 10
lenln equ $-ln

segment .bss
  datos resb 4

segment .text
global _start
_start:

mov eax, 4
mov ebx, 0
mov ecx, mensaje
mov edx, tamanio
int 80h

;leemos del bufer de entrada y guardamos en memoria
mov eax, 3
mov ebx, 0
mov ecx, datos
mov edx, 2
int 80h

mov eax, [datos+0]
sub eax, 48
mov ebx, [datos+1]
sub ebx, 48

mov edi, 10
mul edi
add eax,ebx

mov [arre], eax

;calculamos la serie de numeros felices

mov ebp, arre
mov edi, 0
ciclofeliz:
 mov ah, 0
 mov al, [ebp+edi]

 mov dl, 10
 div dl

 mov [datos+0], al
 mov [datos+1], ah

 mov ax, [datos+0]
 mul ax
 mov cx, ax

 mov ax, [datos+1]
 mul ax

 add cx,ax

mov [ebp+edi+1], cx
inc edi
cmp edi, len
jb ciclofeliz

;imprimimos en pantalla la serie  de numeros
mov ecx, 62
mov [datos+2],ecx

mostrarnumeros:
mov ebp, arre
mov edi, 0

ciclo:
mov ah,0
mov al, [ebp+edi]

mov dl, 10
div dl

add al, '0'
mov [datos+0], al

add ah, '0'
mov [datos+1], ah

mov eax, 4
mov ebx, 0
mov ecx, datos
mov edx, 3
int 80h

add edi, 1 ; aumentamos en uno el valor del contador
cmp edi,len
; preguntamos si ya hemos impreso todos los
; datos del arreglo
jb ciclo


salir:
mov eax, 1
xor ebx, ebx
int 0x80