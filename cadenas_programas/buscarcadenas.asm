segment .data
array1 db 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
len equ $-array1

letrero1 db "no se encontro "
lenletrero1 equ $-letrero1

letrero2 db "se encontro "
lenletrero2 equ $-letrero2

segment .text
global _start
_start:
cld   ; NO LO OLVIDE!!!
mov edi, array1
mov ecx, 10
mov al, 20; dato a buscar

repite:
   scasb
   je encontrado
loop repite
  mov eax, 4
  mov ebx, 0
  mov ecx, letrero1
  mov edx, lenletrero1
  int 80h
  jmp salir

encontrado:
  mov eax, 4
  mov ebx, 0
  mov ecx, letrero2
  mov edx, lenletrero2
  int 80h

salir:
mov eax, 1
xor ebx, ebx
int 0x80