segment .data
array1 db 7, 2, 3, 4, 5, 6, 13, 8, 9, 10
len equ $-array1

array2 db 1, 2, 3, 4, 9, 6, 7, 8, 9, 10
len2 equ $-array2

letrero1 db "[no igual] "
lenletrero1 equ $-letrero1

letrero2 db "[igual] "
lenletrero2 equ $-letrero2

ln db 10,13
lenln equ $-ln

segment .bss
  array3 resb 10
  datos resb 4

segment .text
global _start
_start:
cld   ; NO LO OLVIDE!!!
mov esi, array1
mov edi, array2
mov ecx, 10

repite:
;mov ebp, ecx
push ecx
   cmpsb
   jne noigual
    mov eax, 4
	mov ebx, 0
	mov ecx, letrero2
	mov edx, lenletrero2
	int 80h
    jmp algo
noigual:
mov eax, 4
mov ebx, 0
mov ecx, letrero1
mov edx, lenletrero1
int 80h
algo:
;mov ecx, ebp
pop ecx
loop repite




salir:
mov eax, 1
xor ebx, ebx
int 0x80