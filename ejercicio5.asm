SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "ingrese 5 números", 0xA,0xD
	len1 equ $- msg1
	msg2 db "el numero es impar", 0xA,0xD
	len2 equ $- msg2
	msg3 db "el numero es par", 0xA,0xD
	len3 equ $- msg3

segment .bss
	num1 resb 2

section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point

    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg1
	mov edx, len1
	int 0x80

mov ecx, 5
mov di, 2
for:
    mov esi, ecx

    mov eax, SYS_LEE
	mov ebx, STDIN
	mov ecx, num1
	mov edx, 2
	int 0x80

    mov ax, [num1]
    mov dx, 0
    div di

    cmp dx,0
    je espar

	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg2
	mov edx, len2
	int 0x80
	jmp otracosa
espar:
	mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg3
	mov edx, len3
	int 0x80
otracosa:
	mov ecx, esi

   loop for

salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80