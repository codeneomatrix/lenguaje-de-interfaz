%macro escribe 2 ; la macro se llama escribe y recibe dos parametros
 mov eax, 4
 mov ebx, 1
 mov ecx, %1 ; %1 hace referencia al primer
 ; parametro pasado a la macro
 ; que en este caso especifico es la etiqueta del mensaje
 mov edx, %2 ; %2 hace referencia al segundo6.2 Macros 27
 ; parametro, que en este caso especifico es la cantidad de
 ; caracteres a imprimir
 int 0x80
 %endmacro ; fin de la macro


 segment .data

 msg1 db "mensaje1", 0xA
 len1 equ $-msg1

 msg2 db "mensaje2", 0xA
 len2 equ $-msg2


 segment .text
 global _start
 _start:

 escribe  msg1, len1 ; se invoca a la macro escribe y se le pasan
 ; los parámetros correspondientes etiqueta
 ; y cantidad de caracteres

 escribe msg2, len2   ; se invoca de nuevo a la macro escribe ahora
 ; con distintos valores a la anterior invocación

 salir:
 mov eax, 1
 xor ebx, ebx
 int 0x80