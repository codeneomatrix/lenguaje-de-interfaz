## Reto 1
Realice en ensamblador la formula del estudiante 
$$x = \frac{-b + \sqrt{b^2 - 4ac} }{2a}$$
Tome en cuenta que:
- El calculo de la raiz no tiene que ser exacta , puede usar el metodo o algoritmo babilónico, que arroja una aproximacion.
- Las potencias se pueden expresar como multiplicaciones 
$$x2 = x*x$$
y
$$x3 = x*x*x$$
- En ensamblador las divisiones son enteras, osea:
 1/3 = 0
- Puede usar mul y div , usando registros y numeros enteros , osea:
mul eax, ecx
o
mul eax, 5
- No es necesaria la lectura de teclado
- Puede utilizar, si asi lo desea todos los espacios de memoria que considere necesario.


### Condiciones de entrega
Subira el ejecutable a su cuenta de github, el cual tendra el siguiente nombre:
estuadiante[numerodecontrol], ejemplo: estudiante12161219 ; ademas del codigo.


## Reto 2
En un ataque zombie, dada una poblacion de individuos *n* y tomado en cuenta que un zombie en un dia puede morder a una persona y volverla zombie , ademas de que una persona en un dia puede matar a un numero *m*  (o sea 1 o 2, este numero puede variar) de zombies. Escriba en ensamblador un programa que muestre el numero de zombies y de personas en el transcurso de los dias.

adicional:
- En este video se aborda el tema de forma, un poco mas realista (y de paso demuestra que nos falta estudiar un poquito mas)
https://www.youtube.com/watch?v=NbxxVQi0vQg

## Reto 3
Los numero felices son aquellos que al multiplicar las cifras que los conforman y sumar esos productos, despues de *n* iteraciones el resulatdo sera 1.
Dado un numero cualquiera (del 10 hasta el 44) introducido por teclado, imprimir en pantalla si el numero es feliz o no. 

Tome en cuenta lo siguiente :
- Los numeros felices se explican en el siguiente video:
https://www.youtube.com/watch?v=R2D8SuRjtnQ
- El numero *n* es un numero entre 10 y 44 , que se introducira por teclado
- Puede utilizar todos los registros que considere necesarios
- Puede utilizar todo el espacio de memoria que considere necesario


Ejemplo:
``` bash
~/Desktop $ ./examen4 
23
Es feliz 
~/Desktop $ ./examen4 
14
No es feliz
```

## Condiciones de entrega
Subira el ejecutable a su cuenta de github el cual tendra el siguente nombre :
feliz[numerodecontrol]
ejemplo
feliz12161219
ademas del codigo.

