segment .data
  msg1 db "La conjetura de Collatz,  ingrese un numero y presione enter:", 0xA
  len1 equ $-msg1

  espacio db " "
  len2 equ $-espacio

segment .bss
  resultado resb 2

segment .text
  global _start
_start:

  mov di, 2
  mov si, 0

  mov eax, 4
  mov ebx, 1
  mov ecx, msg1
  mov edx, len1
  int 0x80

  mov eax, 3
  mov ebx, 0
  mov ecx, resultado
  mov edx, 1
  int 0x80

  mov si, [resultado]
  sub si, '0'

  ciclo:
    mov ax, si
    mov dx, 0
    div di

    cmp dx,0
    je otracosa

      mov ax, si
      mov bx, 3
      mul bx
      add ax, 1

    otracosa:
      mov si, ax
      add ax, '0'
      mov [resultado], ax

      mov eax, 4
      mov ebx, 1
      mov ecx, resultado
      mov edx, 2
      int 0x80

      mov eax, 4
      mov ebx, 1
      mov ecx, espacio
      mov edx, len2
      int 0x80

      cmp si, 1
  jne ciclo

  salir:
    mov eax, 1
    xor ebx, ebx
    int 0x80