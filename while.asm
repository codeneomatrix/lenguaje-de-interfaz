SYS_SALIDA equ 1
SYS_LEE equ 3
SYS_PRINT equ 4
STDIN equ 0
STDOUT equ 1

segment .data
	msg1 db "ebx vale :", 0xA,0xD
	len1 equ $- msg1
	msg2 db "ingrese numero2", 0xA,0xD
	len2 equ $- msg2

segment .bss
	num1 resb 1


section  .text
	global _start  ;must be declared for using gcc
_start:  ;tell linker entry point



 mov eax, 3 ; igualamos a eax con tres (eax = 3)
do_while: ; etiqueta que puede ser cualquier cadena
  add ebx, 2 ; aumenta en dos el valor de ebx (ebx+=2)
  sub eax, 1 ; disminuye en uno el valor de eax (eax -= 1)
  cmp eax, 0 ; se compara eax con cero para poder tomar
;acciones posteriores
ja do_while ; si eax es mayor a cero salta a la
; etiqueta do-while

    add ebx, '0'
    mov [num1], ebx

    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, msg1
	mov edx, len1
	int 0x80

    mov eax, SYS_PRINT
	mov ebx, STDOUT
	mov ecx, num1
	mov edx, 1
	int 0x80


salir:
    mov eax, SYS_SALIDA
	xor ebx, ebx
	int 0x80