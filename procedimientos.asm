segment .data

msg1 db "mensaje1", 0xA
len1 equ $-msg1

msg2 db "mensaje2", 0xA
len2 equ $-msg2

segment .text

imprime: ; el procedimeto se identifica con una etiqueta
 mov eax, 4
 mov ebx, 1
 int 0x80
ret ; fin del procedimiento, ret sirve para retornar
 ; a la linea de codigo donde fue llamado si no
 ; lo colocamos ejecutar de nueva cuenta las
 ; instrucciones de la linea 21, 22, 24 , etc

 global _start
 _start:

 mov ecx, msg1
 mov edx, len1
 call imprime ; saltamos a la linea 12, donde se definio el
 ; procedimiento imprime

 mov ecx, msg2
 mov edx, len2
 call imprime ; saltamos a la linea 12, donde se definio el
 ; procedimiento imprime

 salir:
 mov eax, 1
 xor ebx, ebx
 int 0x80